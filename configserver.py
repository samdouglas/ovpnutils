import click
import flask
import random
import logging


app = flask.Flask(__name__)

# Silence Werkzeug log messages, unless they are serious
logging.getLogger('werkzeug').setLevel(logging.ERROR)

PRODUCT_KEY_CHARACTERS = "2346789BCDFGHJKMPQRTVWXY"
SECRET_LENGTH = 5

secret_code = "".join(random.choice(PRODUCT_KEY_CHARACTERS) for i in range(5))

config_data = None
config_attachment_name = None

page_template = """
<html>
    <body>
        <h1>OpenVPN config</h1>
        <p>
            This will download an OpenVPN config file that will allow access to
            the VPN.
        </p>
        <p>
            Anybody with a copy of this config file can gain access to the
            network, so it should be kept safe.
        </p>

        <a href="?download=True">Download config</a>
    </body>
</html>
"""

@app.route("/<secret>")
def get_config(secret):
    if secret.upper().strip() != secret_code:
        flask.abort(403)

    if not flask.request.args.get("download"):
        return page_template, 200
    else:
        return config_data, 200, {
            "Content-Disposition": "attachment; filename=%s" % config_attachment_name
        }


@click.command()
@click.argument("input", default="-", type=click.File("rb"))
@click.option("--host", default="localhost", help="Hostname to listen on")
@click.option("--port", default=6789, help="Port to bind to. Defaults to 6789")
@click.option("--config-name", default="config.ovpn", help="Attachment filename. Defaults to config.ovpn")
def cli(input, host, port, config_name):
    """
    Run an HTTP server to transmit an OpenVPN config file

    This will launch a temporary HTTP server that will host an OpenVPN config
    file (read from STDIN, or a provided filename) so that it can be easily
    transferred to a client.
    """

    global config_data
    global config_attachment_name

    if not app.config.get("SERVER_NAME"):
        app.config["SERVER_NAME"] = host + ":" + str(port)

    config_attachment_name = config_name
    config_data = input.read()
    if not config_data.strip():
        raise click.UsageError("Cannot serve an empty config file");

    with app.app_context():
        print "Go to %s for config" % flask.url_for("get_config", secret=secret_code)

    app.run(host=host, port=port)


if __name__ == "__main__":
    cli()
