import click
import os.path


@click.command()
@click.argument("certificate", type=click.Path(exists=True, readable=True, file_okay=True, dir_okay=False, resolve_path=True))
@click.argument("key", default=None, required=False, type=click.Path(exists=True, readable=True, file_okay=True, dir_okay=False, resolve_path=True))
@click.option("--ca", required=True, type=click.File("r"), help="CA certificate filename")
@click.option("--hostname", required=True, help="Hostname for the VPN server")
@click.option("--port", default=1194, help="Port for the VPN server")
@click.option("--verbosity", default=3, help="Client logging verbosity")
@click.option("-o", "--output", default="-", type=click.File("wb"), help="Output config to file")
@click.option("--config-template", type=click.File("r"), help="Use a custom config template")
def cli(certificate, key, ca, hostname, port, verbosity, output, config_template):
    """
    Create an OpenVPN client config with embedded keys/certs.

    The VPN gateway hostname, CA certificate, and client certificate must be
    provided.

    The client key is optional. If it's not provided, then the filename will
    be inferred from the certificate filename.

    By default the config will be written to stdout, however the --output
    option can write this to a file.

    Example:

        $ ovpn-client-config --hostname "vpn.foo.net" --ca ca.crt client.crt

    A custom config template file can be provided with the --config-template.
    This should use Python % formatting syntax, for example %(hostname)s.

    Template variables: hostname, port, verbosity, ca_certificate, client_key,
    client_certificate.
    """
    template = config_template.read() if config_template else CLIENT_CONFIG_TEMPLATE
    ca_certificate = ca.read()

    cert_root, ext = os.path.splitext(certificate)
    if ext != ".crt":
        raise click.BadParameter("Filename must end with .crt", param_hint="certificate");

    key_file = key if key else cert_root + ".key"

    with open(certificate, "r") as f:
        client_certificate = f.read()

    with open(key_file, "r") as f:
        client_key = f.read()

    config_file = template % {
        "hostname": hostname,
        "port": port,
        "verbosity": verbosity,
        "ca_certificate": ca_certificate,
        "client_key": client_key,
        "client_certificate": client_certificate
    }

    output.write(config_file)
    output.close()


CLIENT_CONFIG_TEMPLATE = """client
proto udp
dev tun
remote %(hostname)s %(port)s
comp-lzo
verb %(verbosity)s

# Keep trying to access the OpenVPN server in case a network
# connection is down
resolv-retry infinite
persist-tun
persist-key

remote-cert-tls server

<ca>
%(ca_certificate)s
</ca>

<cert>
%(client_certificate)s
</cert>

<key>
%(client_key)s
</key>
"""


if __name__ == "__main__":
    cli()
