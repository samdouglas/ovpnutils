from setuptools import setup

setup(
    name="ovpnutils",
    version="0.1",
    author="Sam Douglas",
    author_email="sam.douglas32@gmail.com",
    license="MIT",
    install_requires=["flask", "click"],
    py_modules=["configserver", "clientconfig"],
    entry_points={
        "console_scripts": [
            "ovpn-config-server=configserver:cli",
            "ovpn-client-config=clientconfig:cli"
        ]
    }
)
