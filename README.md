ovpnutils
=========

This is a small collection of scripts for managing an OpenVPN server. It's not
a comprehensive set, but maybe there is something useful.

Currently available scripts:

 * `ovpn-client-config`: Generates a client config with embedded certs and
   keys, intended to be used with Android OpenVPN apps
 * `ovpn-config-server`: Serves an OpenVPN config using a temporary webserver
   to conveniently copy the config onto another computer.

